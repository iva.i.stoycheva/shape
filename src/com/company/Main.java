package com.company;

import java.text.DecimalFormat;

public class Main {

    public static void main(String[] args) {
	// write your code here
        GeometricObject o = new GeometricObject();
        o.display();


        Sphere sphere = new Sphere(5);
        sphere.display();

        Ball ball = new Ball(4, 1);
        ball.display();
    }

}