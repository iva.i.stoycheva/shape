package com.company;

import java.text.DecimalFormat;

public class GeometricObject {
    private static DecimalFormat df = new DecimalFormat("0.00");

    protected double  volume;

    public GeometricObject() {
        this.volume = 1;
    }

    public void display(){
        System.out.println("I'm geometricObject and my volume is " + df.format(volume));
    }
}
