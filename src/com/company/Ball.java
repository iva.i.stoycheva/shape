package com.company;

import java.text.DecimalFormat;

public class Ball extends Sphere{
    private static DecimalFormat df = new DecimalFormat("0.00");

    protected double pressure;
    public Ball( double radium, double pressure) {
        super(radium);
        this.pressure = pressure;
    }

    @Override
    public void display() {

        System.out.println("I'm ball with the pressure " + df.format(pressure) + " and my volume is " + df.format(this.volume()));
    }
}
