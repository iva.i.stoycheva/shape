package com.company;
import java.lang.Math;
import java.text.DecimalFormat;

public class Sphere extends GeometricObject{

    private static DecimalFormat df = new DecimalFormat("0.00");


    protected double radius;
    
    public Sphere(double radius) {
        super();
        this.radius = radius;
    }

    public double volume(){
        return volume = 4/3 * Math.PI * Math.pow(radius, 3);
    }
    public void display(){
        double volume = this.volume();

        System.out.println("I'm sphere and my volume is " + df.format(volume));


    }
    
}
